Xcode Themes used at Agbo
==========================

##FRR Xcode Theme

My bread and butter theme. Uses [the Inconsolata-dz font][inconsolata].

![frr][frr]

###Screencast Presentation

Similar but with larger font size.

##DarkSide Xcode Theme

A darker theme using my new *perfect coding font*: [Anonymous Pro][Anonymous]

![darkSide][darkside]


###DarkSidePresentation

Similar but with larger font size.


[Inconsolata]:http://nodnod.net/2009/feb/12/adding-straight-single-and-double-quotes-inconsola/
[Anonymous]: http://www.marksimonson.com/fonts/view/anonymous-pro

[frr]:https://bitbucket.org/agbo/xcode-themes/raw/47ffa2eb65c2fe5f85382792c16fe6546d32592a/frr.png
[darkside]:https://bitbucket.org/agbo/xcode-themes/raw/47ffa2eb65c2fe5f85382792c16fe6546d32592a/darkSide.png

##Installation

Copy the `dvtcolortheme` files into your `~/Library/Developer/Xcode/UserData/FontAndColorThemes/` folder.

    mkdir -p ~/Library/Developer/Xcode/UserData/FontAndColorThemes/
    cp *.dvtcolortheme ~/Library/Developer/Xcode/UserData/FontAndColorThemes/
